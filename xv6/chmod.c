#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
  if (argc < 3) {
    printf(1, "Not enough parameters.\n");
    printf(1, "Usage: chmod <filename> <mode>\n");
    exit();
  }

  chmod(argv[1], atoi(argv[2]));
  exit();
}
